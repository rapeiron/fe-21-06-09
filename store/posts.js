export const state = () => ({
  list: [],
  filter: '',
  filteredList: []
})

export const mutations = {
  save (state, post) {
    if (!state.list) {
      state.list = []
    }
    const fP = state.list.find(p => +p.id === +post.id)
    if (fP && fP.id && +fP.id === +post.id) {
      fP.text = post.text
    } else {
      state.list.unshift({
        text: post.text,
        id: Date.now()
      })
    }
    state.filteredList = state.list.filter(item => item.text.includes(state.filter))
  },
  remove (state, post) {
    if (!state.list) {
      state.list = []
    }
    state.list = state.list.filter(item => item.id !== post.id)
    state.filteredList = state.list.filter(item => item.text.includes(state.filter))
  },
  setFilter (state, text) {
    state.filter = text
    state.filteredList = state.list.filter(item => item.text.includes(state.filter))
  }
}
